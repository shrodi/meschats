import 'react-native-gesture-handler';
import React, { useState, useEffect } from 'react';
import AsyncStorage from '@react-native-community/async-storage';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import Accueil from 'screens/Accueil'

const Stack = createStackNavigator();

export default function App() {
	

  return (
	<NavigationContainer>
		<Stack.Navigator>
			<Stack.Screen
				name="Accueil"
				component={Accueil}
				options={{ title: 'Bienvenue' }}
			/>
		</Stack.Navigator>
	</NavigationContainer>
  );
}



/*
    Composante:
      Chat

    Props:
      nom         --> Nom du chat

    Événements:
      onClick     --> _miaule
*/

import React, { useState, useEffect, useRef } from 'react';
import { Text, View, Image, Animated, TouchableWithoutFeedback, Dimensions } from 'react-native';
import { Audio } from 'expo-av';
import { Asset } from "expo-asset";

const Chat = (props) => {

    const [ready, setReady] = useState(false);
    const [image, setImage] = useState(null);
    const [miaulement, setMiaulement] = useState(null);
    const [scaleX, setScaleX] = useState(1);
    const [x, setX] = useState(props.position.x);
    const [y, setY] = useState(props.position.y);

    // Crée une animation pour bouger le chat
    const bougeAnimation = useRef(new Animated.ValueXY({ x: 0, y: 0 })).current;

    // Charge l'image de chat comme Asset une seule fois, sur chargement de la composante
    useEffect(() => {
        let mounted = true;
        (async () => {
            const [imageL] = await Asset.loadAsync(require("assets/p_cat1.png"));
            setImage(imageL);

            const [sonL] = await Asset.loadAsync(require("assets/Meowing-cat-sound.mp3"));
            setMiaulement(await Audio.Sound.createAsync(sonL));

            setScaleX(-1);

            setReady(true);
        })();

        return function cleanup() {
          mounted = false
        }
      }, []);

    // Renverse l'image toutes les 200 ms
    useEffect(() => {
        let tO = null;
        if (image != null) {
          tO = setTimeout(_flip, 200);
        }

        return function cleanup() {
          clearTimeout(tO);
        }
    }, [scaleX]);

    // Positionne le chat selon une commande externe
    useEffect(() => {
      // On reçoit une commande de positionnement externe
      if (props.position.externe == true) {
        //console.log("posx, posy:" + props.position.x +","+props.position.y);
        bougeChat(props.position.x, props.position.y);
      }

      return function cleanup() {
        props.position.externe = false;
      }
  }, [props.position]);

    // Bouge le chat aux coordonnées fournies ou sinon aléatoires
    const bougeChat = (newX, newY) => {
      
      const maxX = Math.round(Dimensions.get('window').width / 2) - (props.taille);
      const maxY = Math.round(Dimensions.get('window').height / 2) - (props.taille);
      
      if (newX === undefined || newY === undefined) {
        props.position.externe = false;

        let newMoveY = 0;
        let newMoveX = 0;

        if (Math.random() < 0.5) {
          newMoveX = -1;
        }
        else {
          newMoveX = 1;
        }

        if (Math.random() < 0.5) {
          newMoveY = -1;
        }
        else {
          newMoveY = 1;
        }

        newX = newMoveX * Math.random() * maxX;
        newY = newMoveY * Math.random() * maxY;
      }

      //console.log("newX, newY:" + newX +","+newY);

      setX(newX);
      setY(newY);

      Animated.spring(bougeAnimation, {
        toValue: {x: newX, y: newY},
        useNativeDriver: false,
      }).start()
    }

    // Renverse l'image
    const _flip = async() => {
        setScaleX(-scaleX);
      };

    // Joue un son de miaulement
    const miaule = async (e) => {
        e.preventDefault();
        await miaulement.sound.replayAsync();
    }

    // Retourne une vue animée du chat
    const _renderImage = () => {
        return (
          <Animated.View style={[bougeAnimation.getLayout()]}>
            <Text>Bonjour, je suis le chat {props.nom}!</Text>
            <TouchableWithoutFeedback onPress={e => {miaule(e); bougeChat()}}>
            <Image
                source={{ uri: image.uri || image.localUri }}
                style={{ width: props.taille, height: props.taille, transform: [{scaleX: scaleX}] }}
            />
            </TouchableWithoutFeedback>
          </Animated.View>
        );
      };

    // Retourne une vue du chat une fois la composante chargée
    return (
      <View >
        {ready && image && _renderImage()}        
      </View>
    );
  }

// Propriétés par défaut
Chat.defaultProps = {
  nom: 'Minou',
  position: {
    x: 0,
    y: 0,
    externe: false
  },
  taille: 200
}

// Exporte la composante
export default Chat;

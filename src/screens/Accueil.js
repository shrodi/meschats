import React, { useState, useEffect } from 'react';
import { StyleSheet, View, Text, TextInput, TouchableWithoutFeedback, Dimensions } from 'react-native';
import { Chat } from '../components';
import AsyncStorage from '@react-native-community/async-storage';
import { setStatusBarBackgroundColor, StatusBar } from 'expo-status-bar';
import { useLinkProps } from '@react-navigation/native';

const Accueil = ({ navigation }) => {

	let tailleChat = Math.round(Dimensions.get('window').width / 7);
	const [chat, setChat] = useState({nom: 'Minou', x: 0, y: 0, externe: false, taille: tailleChat});

	// Exécuté inconditionnellement une fois la composante chargée
	useEffect(() => {
		_loadChat();
      }, []);

	// Exécuté chaque fois que chat est changé
	useEffect(() => {
		_saveChat();
    }, [chat]);

	// On enregistre le nouveau nom du chat
	const _saveChat = async () => {
	  try {
		await AsyncStorage.setItem('chat', JSON.stringify(chat));
	  } catch (e) {
		console.log('Erreur sauver chaîne: '+e);
	  }
	}

	// On récupère le nouveau nom du chat
	const _loadChat = async (key) => {
	  try {
		let jsonValeur = await AsyncStorage.getItem('chat');
		let monchat = jsonValeur != null ? JSON.parse(jsonValeur) : null;
		if (monchat != null) {
			setChat({...chat, ...monchat});
		}
	  } catch(e) {
		console.log('Erreur _loadChat: '+e);
	  }
	}

	const _changerNom = (nom) => {
		setChat({...chat, nom: nom});
	}

    return (
		// Toute la page est cliquable; un clic en dehors du Chat => le Chat se rend où le clic a été fait
		<TouchableWithoutFeedback onPress={e => {
				// e.pageX != undefined => Web; e.nativeEvent.locationX != undefined => Android
				if (chat.taille > 0 && (e.pageX || e.nativeEvent.locationX)) {
					let newX = 0;
					let newY = 0;

					const maxX = Math.round(Dimensions.get('window').width / 2) - (chat.taille);
					const maxY = Math.round(Dimensions.get('window').height / 2) - (chat.taille);

					// Web
					if (e.pageX != undefined) {
						newX = e.pageX - maxX - chat.taille;
						newY = e.pageY - maxY - chat.taille;						
					}
					// Android
					else if (e.nativeEvent.locationX != undefined) {
						newX = e.nativeEvent.locationX - maxX;
						newY = e.nativeEvent.locationY - maxY;
					}

					// Met à jour l'état «chat», ce qui va automatiquement mettre à jour les props du Chat correspondantes
					setChat({...chat, x: newX, y: newY, externe: true});
				}
			}}>
			<View style={styles.container}>
				<Chat nom={chat.nom} position={{x: chat.x, y: chat.y, externe: chat.externe}} taille={chat.taille} />
				<Text>{"Nom du chat:"}</Text>
				<TextInput
					style={{ height: 40, borderColor: 'gray', borderWidth: 1 }}
					onChangeText={text => _changerNom(text)}
					value={chat.nom}
					/>
				<StatusBar style="auto" />
			</View>
		</TouchableWithoutFeedback>
    );
}

const styles = StyleSheet.create({
    container: {
      flex: 1,
      backgroundColor: '#fff',
      alignItems: 'center',
      justifyContent: 'center',
    },
  });

// Exporte la composante
export default Accueil;
